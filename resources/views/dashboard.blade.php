<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Biblioteca Virtual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.css">
  
    <style>
        .ni{
            padding: .5rem 1rem
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-light navbar-expand-lg mb-5" style="background-color: #e3f2fd;">
        <div class="container">
            <a class="navbar-brand mr-auto" href="#">BV</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register-user') }}">Register</a>
                    </li>
                    @else
                    @if(Auth::user()->id_rol == 1)
                    <li class="nav-item ni">
                        <a class="nav-link" href="{{ route('usuarios') }}">
                            <strong class="font-bold">
                                <i class="fa fa-solid fa-user-plus"></i> Usuarios
                            </strong>
                        </a>
                    </li>
                    <li class="nav-item ni">
                        <a class="nav-link" href="{{ route('libros') }}">
                            <strong class="font-bold">
                                <i class="fa fa-solid fa-book"></i> Libros
                            </strong>
                        </a>    
                    </li>
                    @endif
                    <li class="nav-item ni">
                        <a class="nav-link" href="{{ route('libros_consulta') }}">
                            <strong class="font-bold">
                                <i class="fa fa-search"></i>
                                Consultar Libro
                            </strong>
                        </a>
                    </li>
                    <li class="nav-item ni">
                        <a class="nav-link" href="{{ route('forget_pass') }}">
                            <i class="fa fa-key"></i>Reestablecer Password
                        </a>
                    </li>
                    <li class="nav-item ni">
                        <a class="nav-link" href="{{ route('signout') }}">Logout</a>
                    </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
        <div class="container">
            @yield('content')
        </div>  

    <script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.js"></script>      
</body>
</html>