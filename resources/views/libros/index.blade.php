@extends('dashboard')

@section('content')
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
<input type="hidden" id="url_listado" value="{{ url('list_libro') }}">
<input type="hidden" id="url_guardar" value="{{ url('save_libro') }}">
<input type="hidden" id="url_eliminar" value="{{ url('del_libro') }}">
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<div class="row">
						<div class="col-md-7"></div>
						<div class="col-md-5">
                            <div class="pull-right">
                                <button type="button" class="btn btn-xs btn-primary" data-bs-toggle="modal" data-bs-target="#ModalSave">
									Nuevo
									<i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
					<!-- Inicia Cuerpo  de la Vista -->
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="table-datos">
							<thead>
								<tr>
									<th class="text-center">Clave</th>
                                    <th class="text-center">Titulo</th>
									<th class="text-center">Descripción</th>
									<th class="text-center">Autor</th>
                                    <th class="text-center">Activo</th>
									<th class="text-center">Acciones</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<!-- Fin Cuerpo de la Vista -->
				</div>
            </div>
        </div>
    </div>
</div>    
<!--Modal guardar-->
<div class="modal fade" id="ModalSave" tabindex="-1" aria-labelledby="modalsavelabel" aria-hidden="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalsavelabel">Libros</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Titulo</label>
                    <input type="text" id="titulo_libro" class="form-control">
                </div>
            </div>
            <div class="col-md-6 m-t-lg">
                <div class="form-group">
                    <label>Descripción</label>
                    <input type="text" id="descripcion" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Autor</label>
                    <input type="text" id="autor" class="form-control">
                </div>
            </div>
            <div class="col-md-6 m-t-lg">
                <div class="form-group">
                    <label>Imprenta</label>
                    <input type="text" id="imprenta" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Adjuntar Archivo</label>
                    <input type="file" accept=".jpg" name="file_foto" id="file_foto" />
                </div>
            </div>
            <div class="col-md-6 m-t-lg">
                <div class="pull-right">
                    <label><input type="checkbox" class="i-checks" name="status" id="status"> Status</label>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="saveform">Guardar</button>
      </div>
    </div>
  </div>
</div>     
<div class="modal fade" id="modal_delete" tabindex="-1" aria-labelledby="modaldeletelabel" aria-hidden="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="bootbox-body" style="text-align: center;">¿Desea eliminar el registro?</div>
				<input type="hidden" name="id" id="id">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><li class="fa fa-times"></li> Cancelar</button>
				<button type="submit" class="btn btn-success" id="btn_eliminar"><li class="fa fa-check"></li> Aceptar</button>
			</div>
		</div>
	</div>
</div>   
@endsection
<script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.js"></script>
<script src="{{ asset('js/biblioteca/libros.js') }}"></script>
