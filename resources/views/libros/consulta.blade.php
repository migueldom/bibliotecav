@extends('dashboard')

@section('content')
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
<input type="hidden" id="url_buscar" value="{{ url('buscar_libro') }}">
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<div class="row">
						<div class="col-md-4"></div>
						<div class="col-md-4">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">
                                        Buscar
                                    </span>
                                </div>
                                <input type="text" id="s_book" class="form-control" placeholder="Libro..." aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row bks"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('js/biblioteca/libros_consulta.js') }}"></script>