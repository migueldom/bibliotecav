<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Libro extends Model
{
    //use SoftDeletes;
    protected $table= 'libros';
    public $timestamps = false;
    protected  $primaryKey = 'id_libro';

    protected $fillable = [
        'id_libro', 'id_categoria', 'titulo_libro', 'descripcion', 'autor', 'imprenta', 'formato_adj', 'status',
    ];

    protected $dates = [ 'deleted_at' ];
}
