<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class UsuarioController extends Controller
{
    public function __construct() {
		$this->middleware('auth');
	}
    public function index()
    {
        return view('usuario.index');
    }
    //Lista
    public function list_usuarios() {
		$results = DB::table('users as u')
		->select('u.id', 'rol.rol as rol', 'u.name', 'u.email')
		->leftjoin('rol', 'rol.id_rol', '=', 'u.id_rol')
        ->get();
		return response()->json(['data' =>$results]);
	}
    //Save Usuario
    public function saveUs(Request $request)
    {
        $result = User::create([
            'name' => $request->Input("name"),
            'email' => $request->Input("correo"),
            'id_rol' => $request->Input("rol"),
            'password' => Hash::make($request->Input('password'))
        ]);
        return response()->json(['data' =>$result]);
    }
    //Delete Usuario
    public function destroy($id) {
		$msg = [];
		$usuario = User::find($id);
		DB::beginTransaction();
		try {
			if ($usuario->delete()) {
				$msg = ['status' => 'ok', 'message' => ''];
			}
		} catch (\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$msg = ['status' => 'fail', 'message' => 'No se pudo eliminar , por favor consulte con el administrador del sistema.', 'exception' => $ex->getMessage()];
			return response()->json($msg, 400);
		} catch (\Exception $e) {
			DB::rollback();
			$msg = ['status' => 'fail', 'message' => 'No se pudo eliminar, por favor consulte con el administrador del sistema.', 'exception' => $ex->getMessage()];
			return response()->json($msg, 400);
		} finally {
			DB::commit();
		}
		return response()->json($msg);
	}
    //Roles
    public function list_rol() {
		$results = DB::table('rol')
		->select('rol.id_rol', 'rol.rol', 'rol.status')
		->get();
		return response()->json($results);
	}
}