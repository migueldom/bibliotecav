<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Hash;
use DB;
use Intervention\Image\Facades\Image;
use Session;
use App\Models\Libro;
use Illuminate\Support\Facades\Auth;


class LibroController extends Controller
{
    public function __construct() {
		$this->middleware('auth');
	}
    public function index()
    {
        return view('libros.index');
    }
    
    //Lista
    public function list_libros() {
		$results = DB::table('libros as lib')
		->select('lib.id_libro as id', 'cat.categoria as categoria', 'lib.titulo_libro', 'lib.descripcion','lib.autor','lib.imprenta','lib.formato_adj','lib.status')
		->leftjoin('categoria as cat', 'cat.id_categoria', '=', 'lib.id_categoria')
        ->get();
		return response()->json(['data' =>$results]);
	}
    //Crear
    public function store(Request $request) {
		$libros = new Libro();
		$libros->titulo_libro = $request->Input("titulo_libro");
        $libros->id_categoria = $request->Input("id_categoria");
        $libros->descripcion = $request->Input("descripcion");
        $libros->autor = $request->Input("autor");
        $libros->imprenta = $request->Input("imprenta");
        
		$libros->status = $request->Input("status");
        $foto = $request->file('formato_adj');
        $directorio_articulos = Storage::disk('public')->directories('img');
		if ($directorio_articulos) {
			Storage::disk('public')->makeDirectory('img', 0777, true);
			Storage::disk('public')->makeDirectory('img/libros', 0777, true);
		}
		DB::beginTransaction();
		try {
			if ($libros->save()) {
				$msg = ['status' => 'ok', 'message' => 'Se ha guardado correctamente'];
                $id = $libros->id_libro;
                if (!empty($foto)) {
					$foto_extension = File::extension($foto->getClientOriginalName());
					$nombre_foto = "$id.$foto_extension";
					$file_foto = "img/libros/$nombre_foto";

					Libro::where('id_libro', $id)
						->update([
							'formato_adj' => $file_foto,
						]);
				}
			}
		} catch (\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$msg = ['status' => 'fail', 'message' => 'No se pudo guardar correctamente, por favor consulte con el administrador del sistema.', 'exception' => $ex->getMessage()];
			return response()->json($msg, 400);
		} catch (\Exception $ex) {
			DB::rollback();
			$msg = ['status' => 'fail', 'message' => 'No se pudo guardar correctamente, por favor consulte con el administrador del sistema.', 'exception' => $ex->getMessage()];
			return response()->json($msg, 400);
		} finally {
			DB::commit();
		}

		return response()->json($msg);
	}
    //Eliminar
    public function destroy($id) {
		$msg = [];
		$libro = Libro::find($id);
		DB::beginTransaction();
		try {
			if ($libro->delete()) {
				$msg = ['status' => 'ok', 'message' => ''];
			}
		} catch (\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$msg = ['status' => 'fail', 'message' => 'No se pudo eliminar , por favor consulte con el administrador del sistema.', 'exception' => $ex->getMessage()];
			return response()->json($msg, 400);
		} catch (\Exception $e) {
			DB::rollback();
			$msg = ['status' => 'fail', 'message' => 'No se pudo eliminar, por favor consulte con el administrador del sistema.', 'exception' => $ex->getMessage()];
			return response()->json($msg, 400);
		} finally {
			DB::commit();
		}

		return response()->json($msg);
	}
    //Libro COnsulta
    public function libro_consulta()
    {
        return view('libros.consulta');
    }
    public function buscar_libro($term)
    {
        $results = DB::table('libros')->where('titulo_libro','LIKE','%'.$term.'%')
                      ->get();
		return response()->json($results);
    }

}