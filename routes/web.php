<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\LibroController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/
Route::get('/', function () {
    return view('auth.login');
});
Route::get('/admin', function () {
    return view('auth.login');
});
//----LOGIN
Route::get('dashboard', [CustomAuthController::class, 'dashboard']); 
Route::get('/login', function(){
    return view('auth.login');
});
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registration', [CustomAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');
Route::get('forget_pass', [CustomAuthController::class, 'forget_pass'])->name('forget_pass');
Route::post('forget_pass_res', [CustomAuthController::class, 'customPass'])->name('customPass.custom');
//--------------------------------------------------------
//*****USUARIOS******/
Route::get('usuarios', [UsuarioController::class, 'index'])->name('usuarios');
Route::get('list_usuario', [UsuarioController::class, 'list_usuarios']);
Route::post('save_usuario', [UsuarioController::class, 'saveUs']);
Route::delete('del_usuario/{id}', [UsuarioController::class, 'destroy']);
//*****LIBROS******/
Route::get('libros', [LibroController::class, 'index'])->name('libros');
Route::get('libros_consulta', [LibroController::class, 'libro_consulta'])->name('libros_consulta');;
Route::get('list_libro', [LibroController::class, 'list_libros']);
Route::post('save_libro', [LibroController::class, 'store']);
Route::delete('del_libro/{id}', [LibroController::class, 'destroy']);
Route::get('buscar_libro/{id}', [LibroController::class, 'buscar_libro']);
//*******ROL********* */
Route::get('list_rol', [UsuarioController::class, 'list_rol']);
//***REESTABLECER PASS */
