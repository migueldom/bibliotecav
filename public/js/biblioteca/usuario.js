var dataTable_datos;
var id = 0;
$(document).ready(function() {
    $.ajax({
        url: $("#url_rol").val(),
        type: 'GET',
        dataType: 'json',
        success: function(data) {console.log(data);
            var rol = data;
            var itemrol = [];
            $("#rol").empty();
            for(var x = 0; x < rol.length; x++)
            {
                $("#rol").append('<option value='+rol[x].id_rol+'>'+rol[x].rol+'</option>');
            }

            $("#rol").val(0).change();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var data = jqXHR.responseJSON;
            if (jqXHR.status == 401) {
                location.reload();
            }

        }
    });

    dataTable_datos =
        $("#table-datos").dataTable({
            "bDeferRender": true,
            "iDisplayLength": 10,
            "bProcessing": true,
            "sAjaxSource": $('#url_listado').val(),
            "fnServerData": function(sSource, aoData, fnCallback, oSettings) {
                oSettings.jqXHR = $.ajax({
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "success": fnCallback,
                    "error": function(jqXHR, textStatus, errorThrown) {
                        var data = jqXHR.responseJSON;
                        /*
                        if (jqXHR.status == 401 || jqXHR.status == 500) {
                            location.reload();
                        }*/
                    }
                });
            },
            "bAutoWidth": false,
            //"bFilter": false,
            "aoColumns": [{
                "mData": "id"
            }, {
                "mData": "name",
                "bSortable": true,
            },{
                "mData": "rol",
                "bSortable": true,
            },{
                "mData": "email",
                "bSortable": true,
            }, {
                "mData": "Acciones",
                "bSortable": false,
                "mRender": function(data, type, full) {
                    var bttnDelete = '<button class="btn btn-danger btn-xs" id="bttn_modal_delete" onclick="deuser(' + full.id + ')" title="Eliminar"><i class="fa fa-trash"></i></button>';
                    return bttnDelete;
                }
            }, ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $("td:eq(0), td:eq(2)", nRow).attr('align', 'center');

            },
            "aLengthMenu": [
                [5, 10, -1],
                [5, 10, "Todo"]
            ],
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Registros del _START_ al _END_  total: _TOTAL_ ",
                "sInfoEmpty": "Sin registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });

    $("#saveformU").click(function() {
        var name = $("#nombre").val();
        var correo = $("#correo").val();
        var password = $("#password").val();
        var rol = $('#rol option:selected').val() == undefined ? '' : $('#rol option:selected').val();
        var data = {
            name:name,
            correo:correo,
            password:password,
            rol:rol
        };
        $.ajax({
            url: $("#url_guardar").val(),
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
            },
            data:data,
            dataType: 'json',
            success: function(data) {
                if (data.status == 'fail') {
                    alert(data.message);
                } else {
                    dataTable_datos._fnAjaxUpdate();
                }
    
                $("#ModalSave").modal('hide');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                var data = JSON.parse(jqXHR.responseText);
                if (jqXHR.status == 400) {
                    toastr.error('', data.message);
                }
    
                if (jqXHR.status == 401) {
                    location.reload();
                }
            }
        });
    });
});

function deuser(id)
{
    $.ajax({
        url: $("#url_eliminar").val() + '/' + id,
        type: 'DELETE',
        headers: {
            'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
        },
        dataType: 'json',
        success: function(data) {
            if (data.status == 'fail') {
                alert(data.message);
            } else {
                dataTable_datos._fnAjaxUpdate();
            }

            $("#modal_delete").modal('hide');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
            if (jqXHR.status == 400) {
                toastr.error('', data.message);
            }

            if (jqXHR.status == 401) {
                location.reload();
            }
        }
    });
}