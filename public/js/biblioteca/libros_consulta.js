$(document).ready(function() {
    console.log("QUERY");
    $("#s_book").keypress(function(e) {
        buscarBook(e.key);
    });
});


function buscarBook(e)
{
    $.ajax({
        url: $("#url_buscar").val() + '/' + e,
        type: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
        },
        dataType: 'json',
        success: function(data) {
            console.log(data);
            console.log(data.length);
            if(data.length > 0){
                $(".bks").empty();
                for(var x = 0; x < data.length; x++)
                {
                    $(".bks").append(
                    '<div class="col-xs-4 col-md-4">'+
                        '<div class="card" style="width: 18rem;">'+
                            '<img class="card-img-top" src="'+data[x].formato_adj+'" alt="'+data[x].imprenta+'">'+
                            '<div class="card-body">'+
                                '<h5 class="card-title">'+data[x].titulo_libro+'</h5>'+
                                '<p class="card-text">'+data[x].descripcion+'</p>'+
                                '<p class="card-text">'+data[x].autor+'</p>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
                }
            }else{
                alert("Sin Resultados");
            }
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
            if (jqXHR.status == 400) {
                toastr.error('', data.message);
            }

            if (jqXHR.status == 401) {
                location.reload();
            }
        }
    });
}