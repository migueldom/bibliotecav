var dataTable_datos;
    var id = 0;
$(document).ready(function() {
    
    //Inicio Tabla Libros
    dataTable_datos =
        $("#table-datos").dataTable({
            "bDeferRender": true,
            "iDisplayLength": 10,
            "bProcessing": true,
            "sAjaxSource": $('#url_listado').val(),
            "fnServerData": function(sSource, aoData, fnCallback, oSettings) {
                oSettings.jqXHR = $.ajax({
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "success": fnCallback,
                    "error": function(jqXHR, textStatus, errorThrown) {
                        var data = jqXHR.responseJSON;
                        /*
                        if (jqXHR.status == 401 || jqXHR.status == 500) {
                            location.reload();
                        }*/
                    }
                });
            },
            "bAutoWidth": false,
            //"bFilter": false,
            "aoColumns": [{
                "mData": "id"
            }, {
                "mData": "titulo_libro",
                "bSortable": true,
            },{
                "mData": "descripcion",
                "bSortable": true,
            },{
                "mData": "autor",
                "bSortable": true,
            }, {
                "mData": "Activo",
                "bSortable": true,
                "mRender": function(data, type, full) {
                    var Activo = full.status == 1 ? '<i class="fa fa-check text-navy"></i>' : '<i class="fa fa-times text-navy"></i>';
                    return Activo;
                },
            }, {
                "mData": "Acciones",
                "bSortable": false,
                "mRender": function(data, type, full) {
                    var bttnDelete = '<button class="btn btn-danger btn-xs" id="bttn_modal_delete" onclick="delibro(' + full.id + ')" title="Eliminar"><i class="fa fa-trash"></i></button>';
                    return bttnDelete;
                }
            }, ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $("td:eq(0), td:eq(2)", nRow).attr('align', 'center');

            },
            "aLengthMenu": [
                [5, 10, -1],
                [5, 10, "Todo"]
            ],
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Registros del _START_ al _END_  total: _TOTAL_ ",
                "sInfoEmpty": "Sin registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    //Save
    $('#ModalSave').on('show.bs.modal', function(e) {
        $('#titulo_libro').val('');
        $('#descripcion').val('');
        $('#autor').val('');
        $('#imprenta').val('');
    });
    $('#saveform').click(function() {
        var msg = '';
        var foto = '';
        var formData = new FormData();
        if ($('#file_foto')[0].files.length > 0) {
            foto = $('#file_foto')[0].files[0];
        }
        console.log(foto);
        formData.append('titulo_libro',$('#titulo_libro').val());
        formData.append('id_categoria',1);
        formData.append('descripcion',$('#descripcion').val());
        formData.append('autor',$('#autor').val());
        formData.append('imprenta',$('#imprenta').val());
        formData.append('formato_adj',foto);
        formData.append('status', $('input:checkbox[name=status]:checked').val() == undefined ? 0 : 1);
    
        var get_url = $("#url_guardar").val();
        var type_method = 'POST';
    
        $.ajax({
            async:true,
            url: get_url,
            type: type_method,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache:false,
            data: formData,
            processData: false,
            contentType: false,
            enctype:'multipart/form-data',
            success: function(data) {
                if (data.status == 'fail') {
                    alert(data.message);
                } else {
                    alert(data.message);
                    dataTable_datos._fnAjaxUpdate();
                    $("#ModalSave").modal('hide');
                }
            },
    
            error: function(jqXHR, textStatus, errorThrown) {
                var data = JSON.parse(jqXHR.responseText);
    
                if (jqXHR.status == 400) {
                    toastr.error('', data.message);
                }
    
                if (jqXHR.status == 401) {
                    location.reload();
                }
    
                if (jqXHR.status == 422) {
                    $.each(data.errors, function(key, value) {
                        if (msg == '') {
                            msg = value[0] + '<br>';
                        } else {
                            msg += value[0] + '<br>';
                        }
    
                    });
    
                    toastr.error(msg);
                }
            }
        });
    });
    //Delete
    $("#modal_delete").on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget);
        id = typeof button.data('id') != "undefined" ? button.data('id') : 0;
    });
});

function delibro(id){
    $.ajax({
        url: $("#url_eliminar").val() + '/' + id,
        type: 'DELETE',
        headers: {
            'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')
        },
        dataType: 'json',
        success: function(data) {
            if (data.status == 'fail') {
                alert(data.message);
            } else {
                dataTable_datos._fnAjaxUpdate();
            }

            $("#modal_delete").modal('hide');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
            if (jqXHR.status == 400) {
                toastr.error('', data.message);
            }

            if (jqXHR.status == 401) {
                location.reload();
            }
        }
    });
}

